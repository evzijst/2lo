import getopt
import requests
import sys

from ConfigParser import ConfigParser
from requests_oauthlib import OAuth1


class RedirectingOAuthSession(object):
    """Because:
    https://github.com/requests/requests-oauthlib/issues/94
    """
    def __init__(self, key, secret):
        self.session = requests.Session()
        self.session.auth = OAuth1(key, secret)

    def get(self, url, **kwargs):
        kwargs['allow_redirects'] = False
        for i in xrange(10):
            resp = self.session.get(url, **kwargs)
            if resp.status_code in (301, 302):
                url = resp.headers.get('Location')
                if url:
                    continue
            return resp
        raise requests.TooManyRedirects

    def post(self, url, data=None, **kwargs):
        return self.session.post(url, data=data, **kwargs)


class Run(object):
    usage = """Make a 2-LO request.

Usage: $ %s [OPTION] url

    -c --credentials    path to OAuth credentials file
    -h, --help          print this message and exit.
""" % sys.argv[0]

    def parse_args(self, args):
        key, secret = None, None
        options, self.url = getopt.getopt(args, 'hc:',
                                          ['help', 'credentials='])
        for opt, arg in options:
            if opt in ('-c', '--credentials'):
                cfg = ConfigParser()
                if cfg.read(arg):
                    key = cfg.get('oauth', 'key')
                    secret = cfg.get('oauth', 'secret')
                else:
                    print >> sys.stderr, 'Invalid path:', arg
                    exit(1)
            elif opt in ('-h', '--help'):
                print self.usage
                exit(0)
        if not all((key, secret, self.url)):
            print >> sys.stderr, self.usage
            exit(1)

        self.url = self.url[0]
        self.session = RedirectingOAuthSession(key, secret)

    def run(self):
        resp = self.session.get(self.url)
        if resp.ok:
            print resp.text.encode(resp.encoding)
        else:
            print >> sys.stderr, self.url, 'failed:', ('authorization failed' if
                                                  resp.status_code == 401 else
                                                  resp.text.encode('utf-8'))


if __name__ == '__main__':
    run = Run()
    run.parse_args(sys.argv[1:])
    run.run()
